package helpers

func GeneratePoWChallenge() (string, string) {
	// Generate a random challenge and solution
	challenge := randomString(16)
	solution := GeneratePoWSolution(challenge)
	return challenge, solution
}
