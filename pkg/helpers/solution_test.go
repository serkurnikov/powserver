package helpers

import (
	"testing"
)

const (
	challengeExample = "1FdMz0rxRlcBtXKW"
)

func TestSolution(t *testing.T) {
	solution1 := GeneratePoWSolution(challengeExample)
	solution2 := GeneratePoWSolution(challengeExample)

	if solution1 != solution2 {
		t.Errorf("Solutions do not match. Solution 1: %s, Solution 2: %s", solution1, solution2)
	}
}
