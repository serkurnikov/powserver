package helpers

import (
	"crypto/sha256"
	"encoding/hex"
	"math/rand"
	"strings"
)

const (
	difficulty = 2
)

func GeneratePoWSolution(challenge string) string {
	rand.Seed(int64(42))

	prefix := "0"
	for i := 1; i <= difficulty; i++ {
		prefix += "0"
	}

	for {
		attempt := randomString(16)
		hash := generateHash(challenge + attempt)
		if strings.HasPrefix(hash, prefix) {
			return attempt
		}
	}
}

func randomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	result := make([]byte, length)
	for i := range result {
		result[i] = charset[rand.Intn(len(charset))]
	}
	return string(result)
}

func generateHash(data string) string {
	// Use SHA-256 for hashing
	hasher := sha256.New()
	hasher.Write([]byte(data))
	hashBytes := hasher.Sum(nil)
	return hex.EncodeToString(hashBytes)
}
