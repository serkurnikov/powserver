package main

import (
	"fmt"
	"math/rand"
	"net"
	"strings"

	"gitlab.com/serkurnikov/powserver/pkg/helpers"
)

// Quotes collection
var quoteCollection = []string{
	"Quote 1: Wisdom is the reward you get for a lifetime of listening when you'd have preferred to talk.",
	"Quote 2: The only true wisdom is in knowing you know nothing.",
	"Quote 3: The journey of a thousand miles begins with one step.",
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}
	defer ln.Close()
	fmt.Println("Word of Wisdom Server is listening on :8080")

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("Error: ", err)
			continue
		}
		go handleRequest(conn)
	}
}

func handleRequest(conn net.Conn) {
	defer conn.Close()

	// Generate a random PoW challenge
	challenge, solution := helpers.GeneratePoWChallenge()

	conn.Write([]byte("Welcome to the Word of Wisdom Server!\n"))
	conn.Write([]byte("Please solve this PoW challenge:" + challenge + "\n"))

	// Read the client's PoW response
	buffer := make([]byte, 64)
	_, err := conn.Read(buffer)
	if err != nil {
		fmt.Println("Error reading from client: ", err)
		return
	}

	clientSolution := strings.TrimFunc(string(buffer), removeNonPrintableChars)

	if clientSolution == solution {
		conn.Write([]byte("PoW challenge solved. Here's your quote:\n"))
		randomQuote := getRandomQuote()
		conn.Write([]byte(randomQuote + "\n"))
	} else {
		conn.Write([]byte("PoW challenge failed. Goodbye!\n"))
	}
}

func getRandomQuote() string {
	// Select a random quote from the collection
	randIndex := rand.Intn(len(quoteCollection))
	return quoteCollection[randIndex]
}

func removeNonPrintableChars(r rune) bool {
	return r < 32 || r > 126
}
