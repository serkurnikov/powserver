# Proof of Work (PoW) for Word of Wisdom Server

This documentation explains the choice and implementation of the Proof of Work (PoW) algorithm for the Word of Wisdom Server, which is designed to protect the server from Distributed Denial of Service (DDOS) attacks.

## Algorithm Choice

The selected PoW algorithm is a simple and lightweight one that is suitable for the specific use case of limiting the number of requests clients can make to the server. It offers several advantages:

1. **Resource Intensiveness:** The algorithm requires clients to perform computational work, limiting their ability to send requests. This helps prevent DDOS attacks by increasing the cost for attackers.

2. **Ease of Implementation:** The PoW algorithm is easy to understand and implement. It uses random strings and a hash function to create a solution that matches a specified difficulty level. This ensures simplicity in both deployment and server-side verification.

3. **Customizable Difficulty:** The algorithm's difficulty can be easily configured by adjusting the number of leading zeros required in the hash. This flexibility allows you to balance security and server accessibility by setting the appropriate difficulty level.

4. **Pseudorandomness:** By using `rand.Seed(int64(42))`, the algorithm ensures that the generated PoW solution remains consistent for the same input (challenge). This is crucial for allowing clients to reuse solutions when sending requests.

5. **Limited Request Handling:** The algorithm is well-suited for scenarios where you want to limit the number of requests a client can make. By requiring computational work for each request, it automatically reduces the client's request throughput.

This algorithm is not cryptographically strong and is not suitable for security or digital signature related tasks. 
Your job is to ensure that requests to the server are limited, and this algorithm does that job well.

## Implementation

The PoW algorithm's implementation is as follows:

```go
package helpers

import (
	"math/rand"
	"strings"
)

const (
	difficulty = 2
)

func GeneratePoWSolution(challenge string) (string, string) {
    rand.Seed(int64(42))

    prefix := "0"
    for i := 1; i <= difficulty; i++ {
        prefix += "0"
    }

    for {
        attempt := randomString(16)
        hash := generateHash(challenge + attempt)
        if strings.HasPrefix(hash, prefix) {
            return attempt, hash
        }
    }
}

func randomString(length int) string {
	// Generating random string
	return ""
}

func generateHash(data string) string {
	// SHA-256 for hashing
	return ""
}
```

In this implementation, clients generate PoW solutions by performing computations based on the provided challenge. 
The function returns a solution that matches the specified difficulty level.

## Conclusion

The chosen PoW algorithm effectively limits the number of requests clients can make to the Word of Wisdom Server. 
Its simplicity, customizability, and resource-intensiveness make it well-suited for this specific use case.

## Run localy

### Firstly create binaries for server and client
```bash
@cd server && go build ./server.go
@cd client && go build ./client.go
```

### Launch binaries
```bash
@cd server && go run ./server.go
@cd client && go run ./client.go
```

Logs from server app:
```bash
Word of Wisdom Server is listening on :8080
```

Logs from client app:
```bash
Welcome to the Word of Wisdom Server!
Please solve this PoW challenge:MkNCkmPDLtUV5v03
PoW challenge solved. Here's your quote:
Quote 2: The only true wisdom is in knowing you know nothing.
```
