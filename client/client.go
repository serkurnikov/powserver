package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"

	"gitlab.com/serkurnikov/powserver/pkg/helpers"
)

func main() {
	serverAddress := "localhost:8080"

	conn, err := net.Dial("tcp", serverAddress)
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		return
	}
	defer conn.Close()

	scanner := bufio.NewScanner(conn)

	for scanner.Scan() {
		text := scanner.Text()
		fmt.Println(text)

		if strings.Contains(text, "Please solve this PoW challenge") {
			// Extract the challenge string from the text
			challenge := strings.Replace(text, "Please solve this PoW challenge:", "", 1)

			// Generate a PoW solution using your helpers package
			clientSolution := helpers.GeneratePoWSolution(challenge) // Replace with your PoW solution
			// Send the PoW solution to the server
			conn.Write([]byte(clientSolution))

			// Read and print the server's response
			for scanner.Scan() {
				fmt.Println(scanner.Text())
			}

			break
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading from server:", err)
	}
}
